import Queue
import threading
import os
import urllib2

threads = 10

## READ: Script presumes user knows the CMS of target URL to run against ##

# define target URL and local directory of target web application to compare against // filter unwanted file extensions
target = "http://www.example.com"
directory = "/path/to/application/install" # i.e /users/wordpress
filters = [".jpg", ".gif", ".png", ".css", ".eot", ".woff"]

os.chdir(directory)

# create Queue object to store files we want to locate on remote server
web_paths = Queue.Queue()

# walk through files and directories and build full file paths to test against filters
for r,d,f in os.walk("."):
	for files in f:
		remote_path = "{}/{}".format(r,files)

		if remote_path.startswith("."):
			remote_path = remote_path[1:]

		# check filters - split into pairs
		if os.path.splitext(files)[1] not in filters:
			web_paths.put(remote_path)

# keep executing loop until web_paths queue is empty
def test_remote():

	while not web_paths.empty():
		path = web_paths.get()
		url = "{}{}".format(target, path)

		request = urllib2.Request(url)

#output HTTP status w/ file path or call error if file is not found or protected in .htaccess
		try:
			response = urllib2.urlopen(request)
			content = response.read()

			print "[{}] => {}".format(response.code,path)
			response.close()

		except urllib2.HTTPError as error:
			print "Failed {}".format(error.code)
			pass

for i in range(threads):
	print "Spawning thread: {}".format(i)
	t = threading.Thread(target=test_remote)
	t.start()
