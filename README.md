# README #

This is a Python scripting repo. You'll find recon web application tools, web scrapers and directory traversal scripts, and anything else that comes up related to general Python coolness. Enjoy.

### SETUP ###

Most scripts can be run using 
```
#!command line

python filename.py
```
Where required, python3 specific scripts will be noted.