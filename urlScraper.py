#!/usr/bin/python

from bs4 import BeautifulSoup
import urllib3
import requests
import urlparse2
import random
import MySQLdb

### run as python3 -- beginnings of a URL difference check tool ###
class Spyder(object):
    def __init__(self):
        self.soup = None
        self.current_url = 'http://example.com'
        self.links = set()
        self.visited = set()

        self.counter = 0

    def scan(self):
        # display current url w/ count
        print(self.counter , ":", self.current_url)

        res = requests.get(self.current_url)
        markup = res.content # read content
        self.visited.add(self.current_url) # add to visited

        # start soup link grab
        self.soup = BeautifulSoup(markup, "html.parser")

        all_links = [] # open list
        try:
            for link in [l.get('href') for l in self.soup.find_all('a')]:
                print("[*] Link found: '" + link + "'")

                if link.startswith('http'):
                    all_links.append(link)
                    print("Adding link " + link + "\n")
                elif link.startswith('/'):
                    parts = urlparse2.urlparse2(self.current_url)
                else:
                    all_links.append(self.current_url + link)
                    print("Adding link " + self.current_url + "\n" + link + "\n")
        except Exception:
            print('[*] Something happened.')

        self.links = self.links.union(set(all_links)) # new set of links

        self.current_url = random.sample(self.links.difference(self.visited), 1)[0]
        self.counter += 1

        head = requests.head(self.current_url) # get header info
        print(head.status_code, head.headers)

    def run(self):
        # open db connection
        db_conn = MySQLdb.connect(host='hostname', user='user', passwd='password', db='dbname')
        c = db_conn.cursor()

        while len(self.visited) < 3:
            self.scan()

        for link in self.links: # run queries on found links
            query = c.execute("""INSERT INTO table_name (column_name) VALUES (%s)""", [link])
            db_conn.commit()

        print("[*] Successfully added URLs to database")

# run it all
if __name__ == '__main__':
    spider = Spyder()
    spider.run()
