# THIS IS A PYTHON 3.3+ SCRIPT

# turn on some python compatibility flags
from __future__ import print_function, unicode_literals
from bs4 import BeautifulSoup
import requests
from colorama import init
import sys

def mail_scraper():
    ## set target url
    target_url = 'http://www.example.com/'

    ## request our data
    response = requests.get(target_url)

    ## parse the data - delicious soup
    sopa = BeautifulSoup(response.content, 'html.parser')


    ## find all hrefs for review first
    for links in sopa.find_all('a'):
        print(links.get('href'))

    
    # display mailto info in links
    email_addy = [a['href'] for a in sopa.select('a[href^=mailto:]')]

    # check if page contains addresses - if no, terminate
    if not email_addy:
        print('\033[31m' + '[*] Sorry, there are no email addresses on this page.')
        sys.exit()
    else:
        print(email_addy)
    
    while True:
        try:
            answer = str(input('\033[33m' + '[*] Would you like to export the email addresses? (y/n): '))
            
        except ValueError:
            print('[*] Please respond yes or no.')
            
        
        if answer.lower() in ['y', 'yes']:
            
     # write email contacts to a file
         try:
            with open('contacts.txt', 'w') as f:
                print(email_addy, file=f)
                print('\033[32m' + '[*] Success! The email addresses have been saved in the current working directory')
                break
         except:
             print('Something happened. That is all we know.')
            
        elif answer.lower() in ['n', 'no']:
            break       

mail_scraper()