from bs4 import BeautifulSoup
import re
import requests
from colorama import init

# make this py 2 & 3 compatible
try:
	from urllib.parse import urlparse
except ImportError:
	from urlparse import urlparse


def quick_search():

	url = 'http://www.example.com/'
	results = requests.get(url)
	data = results.text

	# set words to search
	target_words = ['hello', 'cruel', 'world']

	# start soup - url to parse
	info_soup = BeautifulSoup(data, 'html.parser')

	# return hrefs to reference
	links = [a['href'] for a in info_soup.select('a[href]')]

	# loop through defined words
	for w in target_words:
		output = info_soup.find_all(string=re.compile('.*{}.*'.format(w)), recursive=True)

	# output how many times words occur
		all_words = '\033[33m' + "[*] The word '{}' was found {} times\n".format(w, len(output))
		print (all_words)

	# check user URL
	while True:
		q = str(input('\033[33m' + "Check another URL? (y/n): "))

		if q.lower() in ['y', 'yes']:

			# match url scheme to parse url input
			try:
				prompt = input("[*] Enter a new URL to search (ex. http://...): ")
				response = urlparse(prompt)

				if response.scheme:
					print('ok')

				else:
					print('Sorry, nothing found. Check your URL format.')

			except ValueError:
				print('Please enter a valid URL')

		elif q.lower() in ['n', 'no']:
			break

		else:
			print('\033[31m' + "Please enter yes or no")


quick_search()
