## Use wordlists to bruteforce discover files & directories on a target server
## Specifically looking for debugging files, config files, or other leftovers that may be interesting

import urllib2
import urllib
import threading
import Queue

threads = 5
target_url = "http://example.com"
wordlist_file = "/path/to/wordlist"
resume = None
user_agent = "Mozilla/5.0 (X11; Linux x86_64; rv:19.0) Gecko/20100101 Firefox/19.0"

def build_wordlist(wordlist_file):

	# read the wordlist file and iterate over each line
	fd = open(wordlist_file, "rb")
	raw_words = fd.readlines()
	fd.close()

	found_resume = False

	# return queue of words
	words = Queue.Queue()

	for word in raw_words:

		word = word.rstrip()

		# resume the session if connection is lost or target goes down
		if resume is not None:

			if found_resume:
				words.put(word)
			else:
				if word == resume:
					found_resume = True
					print "Resuming wordlist from: {}".format(resume)
		else:
			words.put(word)

	return words

# add functionality: add extension list to test
def dir_bruter(word_queue,extensions=None):

	while not word_queue.empty():
		attempt = word_queue.get()

		attempt_list = []

		# check for file extension. if not, brute the directory
		if "." not in attempt:
			attempt_list.append("/{}/".format(attempt))
		else:
			attempt_list.append("/{}".format(attempt))

		# maybe we want to brute extensions..take current word & apply file extension we want to test
		if extensions:
			for extension in extensions:
				attempt_list.append("/{}{}".format(attempt, extension))

		# iterate over list of attempts
		for brute in attempt_list:
			url = "{}{}".format(target_url, urllib.quote(brute))

		# set innocuous user-agent headers
			try:
				headers = {}
				headers["User-Agent"] = user_agent
				r = urllib2.Request(url, headers=headers)

				response = urllib2.urlopen(r)

			# if 200, output the url
				if len(response.read()):
					print "{} => {}".format(response.code, url)

			# output anything but 404 really
			except urllib2.URLError,e:
				if hasattr(e, "code") and e.code != 404:
					print "!!! {} => {}".format(e.code, url)

				pass

word_queue = build_wordlist(wordlist_file)
extensions = [".php", ".bak", ".orig", ".inc"]

for i in range(threads):
	t = threading.Thread(target=dir_bruter, args=(word_queue, extensions,))
	t.start()
